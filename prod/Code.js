function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('Options')
    .addItem('Process Incentives', 'processIncentives')
    .addSeparator()
    .addItem('Import Incentives from Spreadsheet', 'importIncentiveFromSS')
    .addSeparator()
    .addItem('Perks Cap Checker', 'perksCapCheck')
    // .addSeparator()
    // .addItem('Check Incentives Version', 'getIncentivesVersion') removed per b/192574089
    .addToUi();
}
