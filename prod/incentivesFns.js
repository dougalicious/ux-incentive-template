function processIncentives() {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = ss.getSheetByName("Incentives");
  const name = sheet.getName()
  let sessionEmail = Session.getActiveUser().getEmail();
  try {
    ProjectOrionLibrary
  } catch (e) {
    debugger
    throw (e)
  }
  let lock = LockService.getScriptLock();
  lock.waitLock(5000);
  typeof ProjectOrionLibrary === 'object' && ProjectOrionLibrary.processIncentives({ sheet, sessionEmail });
  lock.releaseLock();
  SpreadsheetApp.flush()
}

function importIncentiveFromSS() {
  try {
    ProjectOrionLibrary
  } catch (e) {
    throw (e)
  }
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = ss.getActiveSheet();
  ProjectOrionLibrary.importIncentives(sheet)
  SpreadsheetApp.flush()
}

function getIncentivesVersion(){
  try {
    ProjectOrionLibrary
  } catch (e) {
    throw (e)
  }
  
  let versionNum = ProjectOrionLibrary.getIncentiveVersion();
  Browser.msgBox(`Incentives Version #${versionNum}`)
}