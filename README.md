# UX Incentive Template - 2021

**Status:** *[see below for values]* (as of 2021-08-18)

[Douglas Cox](http://who/douglascox) [douglascox@]

**Contents**
[UX Incentive Template - 2021](https://docs.google.com/spreadsheets/d/1ojZ6wzUX8g9yFpjY7rGPK39l9Warj1OppvWkft4euys/edit#gid=140392909)

[TOC]

*Notes:*

*  This Trix and associated GAS(Google Apps Script) functionality is used specifically by the Incentives team, although some of this same functionatily is within the [[Google] Orion Study Spreadsheet Template (go/orion-study-sheet)](https://docs.google.com/spreadsheets/d/1swcx4NzjriKpJWAE-2CnqkKJ0R2p5KkHZBhmcZLucAI/edit#gid=1054252732) the Incentive team sort of leads the development and feature updates of the Incentives Library. 

## Objective {#objective}

*Provides a means for the Incentives team to process Incentives via GAS(Google Apps Script), across various Incentive types. after running process incentives this then updates the relavent trix with new data*

## Background {#background}

*The previous version of these functionality were instituted utilizing add-ons. This current version reference GAS libraries. Various updates have been added, bugs found, and resolved since its implementation*

## Overview {#overview}

*Allows the incentives team to "Process Incentives", "Import Incentives from Spreadsheet", "Perks Cap Checker". These menu options buttons correlat to two GAS Libraries.*
***not*** *working on the project.*

## Infrastructure {#infrastructure}

*By way of the option menu, triggers the execution of the GAS script*
* Libraries
  * [ProjectOrionLibrary](https://script.google.com/corp/home/projects/1toPvXCA1lIDyGyhFAei5dP3i1zMEevjN7bM77YxW9tqD_UkL6jK5UK-d/edit) - contains Incentive's specific action
  * [OrionLibraryV2](https://script.google.com/corp/home/projects/1xZ1r8SYZepq5LhnwC8NWuuj3B1K-qHrsgmS_VVdBJjTjVEJIb51kmMYh/edit) - contain a specific Study Template's action
* Services
  * AdminDirectory - doesn't appear to be used anywhere, may be remnant of earlier version

## Detailed Design {#design}

*Through the options menu an action is selected. The action then references the associating code library to execute. For the Incentive's task, the "Processed Status" is updated with status either success or fail. With the "Perks Cap Checker" this code looks to evaulate other trixes to get information*

## Project Information {#project}

*Please provide the following information about the project:*

* 'Process Incentives' - Process data from the "Incentives" Sheet relative to the incentive type. Based on incentive type various actions are done please see Library info for details [IncentiveLibrary](https://bitbucket.org/dougalicious/incentivelibrary/src/master/)
* 'Import Incentives from Spreasheet'
* 'Perks Cap Chcker'

## Document History {#document_history}


**Date** | **Author** | **Description** | **Reviewed by** | **Signed off by**
-------- | ---------- | --------------- | --------------- | -----------------
2021-08-18      | douglascox@        | add README | ...             | ...
...      | ...        | Dummy entry for | ...             | ...
...      | ...        | Dummy entry for | ...             | ...

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact